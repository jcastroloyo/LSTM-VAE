Este repositorio contiene los conjuntos de datos utilizados para el entrenamiento y evaluación del trabajo de fin de máster de Juan Castro Loyo titulado *Detección de anomalías en minería de procesos basada en técnicas de aprendizaje profundo*.

La arquitectura VAE-LSTM presentada puede encontrarse en un Notebook de Google colab ubicado en:

 https://colab.research.google.com/drive/11dJNSdMdC2_xvIgNZy7ediBdv_mVBrAH?usp=sharing

A continuación, se detallan los pasos necesarios para probar la arquitectura y replicar los resultados:

## Guia de utilización

1. Descargue los conjuntos de datos de este repositorio

2. Acceda al notebook de google ubicado en: https://colab.research.google.com/drive/11dJNSdMdC2_xvIgNZy7ediBdv_mVBrAH?usp=sharing

3. En el índice del notebook verá “Local system loader" ejecute dicha celda y añada los conjuntos de datos con los que desee trabajar

4. Ejecute todas las celdas posteriores a esta para inicializar las funciones necesarias. Para un entrenamiento y evaluación mínimas se requieren las funciones definidas en las celdas siguientes:
    + Integer encoding and features extractor
    + Data loaders
    + VAE
    + NNLOSS threshold
    + Tran, evaluate and save results

5. En la última sección "Tran, evaluate and save results", añada los nombres de los conjuntos de datos que desea evaluar como entrada a la función "train_save_evaluate_fromLocal" con el formato:
    + datasets: Lista de conjuntos de datos a evaluar, con formato:
            ['dataset1.csv',
             'dataset2.csv',
            ...
            'datasetN.csv' ]
    + task_id: Clave identificativa de la columna de actividad del conjunto de datos. No es necesario para los datasets de este repositorio. **Default:'task'**
    + case_id: Clave identificativa de la columna de caso del conjunto de datos. No es necesario para los datasets de este repositorio. **Default:'id'**
    + epochs: Épocas de entrenamiento que se desean evaluar con el formato:
            ['1',
             '2',
            ...
            '100' ]
     **Default:[350]**
    + latent_factor: Factores latentes que se desean evaluar con el formato:
            ['0.1',
             '0.2',
            ...
            '0.9' ]
     **Default:[0.8]**

Se puede encontrar un ejemplo de la llamada en la última celda del notebook.

6. La función entrenará y evaluará un modelo por conjunto de datos. Los resultados quedarán guardados en Google colab y pueden descargarse desde el menú lateral izquierdo como se muestra en la figura:
![image.png](./image.png)

